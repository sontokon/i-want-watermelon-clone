﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class Fruit : MonoBehaviour
{
    public FruitType FruitType;

    private IObjectPool<Fruit> _pool;

    public void SetPool(IObjectPool<Fruit> pool) => _pool = pool;

    public void Release()
    {
        _pool?.Release(this);
        FruitManager.instance.RemoveFruitFromList(this);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Fruit otherFruit = collision.gameObject.GetComponent<Fruit>();
        if (otherFruit != null && otherFruit.FruitType == FruitType)
        {
            // Tạo một loại quả mới
            if (!FruitManager.hasSpawnedNewFruit)
            {
                //var newFruitType= FruitManager.instance.GetFruitType3();
                var newFruitType = FruitManager.instance.GetNextFruit((int)otherFruit.FruitType + 1);
                newFruitType.transform.position = (otherFruit.transform.position + this.transform.position) /2f;
                FruitManager.hasSpawnedNewFruit = true;
                FruitManager.instance.CallTheCoroutine(); // Đánh dấu là đã sinh ra quả mới
                GameManager.instance.IncreaseScore((int)otherFruit.FruitType);
            }
                
                // Giải phóng quả thứ hai
                Release();              
        }
    }

}

public enum FruitType
{
    type1,
    type2,
    type3,
    type4,
    type5,
    type6,
    type7,
    type8,
    type9,
}
