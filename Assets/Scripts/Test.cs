using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Test : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //SpawnFruit(position);
            GetRandomFruitToSpawn(Random.Range(0, 2), position);
        }
    }

    private void SpawnFruit(Vector2 position)
    {
        //var cac = fruitmanager.instance.getfruittype1();
        //cac.transform.position = position;
        

        var fruit = FruitManager.instance.GetFruitType1();
        FruitManager.instance.shakingRigidbodies.Add(fruit.GetComponent<Rigidbody2D>());
        fruit.transform.position= new Vector2(position.x, Random.Range(4.2f,4.6f));
    }

    public void GetRandomFruitToSpawn(int number, Vector2 position)
    {
        ;
        switch (number)
        {
            case 0:
                var fruit = FruitManager.instance.GetFruitType1();
                FruitManager.instance.shakingRigidbodies.Add(fruit.GetComponent<Rigidbody2D>());
                fruit.transform.position = new Vector2(position.x, Random.Range(4.2f, 4.6f));
                break;
            case 1:
                fruit = FruitManager.instance.GetFruitType2();
                FruitManager.instance.shakingRigidbodies.Add(fruit.GetComponent<Rigidbody2D>());
                fruit.transform.position = new Vector2(position.x, Random.Range(4.2f, 4.6f));
                break;
            case 2:
                fruit = FruitManager.instance.GetFruitType3();
                FruitManager.instance.shakingRigidbodies.Add(fruit.GetComponent<Rigidbody2D>());
                fruit.transform.position = new Vector2(position.x, Random.Range(4.2f, 4.6f));
                break;

        }
    }    
}
