﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using Utils;

public class FruitManager : Singleton<FruitManager>
{
    [SerializeField] private Fruit Prefab1;
    [SerializeField] private Fruit Prefab2;
    [SerializeField] private Fruit Prefab3;
    private ObjectPool<Fruit> poolType1;
    private ObjectPool<Fruit> poolType2;
    private ObjectPool<Fruit> poolType3;

    public static bool hasSpawnedNewFruit = false;
    public List<Rigidbody2D> shakingRigidbodies = new();
    private void Awake()
    {
        poolType1 = new ObjectPool<Fruit>(CreateFruitType1, OnGetFruit1,OnReleaseFruit1);
        poolType2 = new ObjectPool<Fruit>(CreateFruitType2, OnGetFruit2, OnReleaseFruit2);
        poolType3 = new ObjectPool<Fruit>(CreateFruitType3, OnGetFruit3, OnReleaseFruit3);
    }

    public void RemoveFruitFromList(Fruit fruit)
    {
        shakingRigidbodies.Remove(fruit.GetComponent<Rigidbody2D>());
    }
    IEnumerator DelayUntilNextSpawn()
    {
        yield return new WaitForFixedUpdate();
        hasSpawnedNewFruit = false;
    }
    public void CallTheCoroutine()
    {
        StartCoroutine(DelayUntilNextSpawn());
    }
    private void OnReleaseFruit3(Fruit fruit3)
    {
        fruit3.gameObject.SetActive(false);
    }

    private void OnGetFruit3(Fruit fruit3)
    {
        fruit3.gameObject.SetActive(true); 
    }

    private Fruit CreateFruitType3()
    {
        var fruit3 = Instantiate(Prefab3);
        fruit3.SetPool(poolType3);
        return fruit3;
    }

    private void OnReleaseFruit2(Fruit fruit2)
    {
        fruit2.gameObject.SetActive(false);
        fruit2.transform.position = Vector3.zero;
    }

    public Fruit CreateFruitType2()
    {
        var fruit2 = Instantiate(Prefab2);
        fruit2.SetPool(poolType2);
        return fruit2;
    }

    private void OnGetFruit2(Fruit fruit2)
    {
        fruit2 .gameObject.SetActive(true);
    }

    private void OnReleaseFruit1(Fruit fruit)
    {
        fruit.gameObject.SetActive(false);
        //fruit.transform.position = Vector3.zero;

    }

    private void OnGetFruit1(Fruit fruit)
    {
       fruit.gameObject.SetActive(true);
  
    }

    private Fruit CreateFruitType1()
    {
        var fruit = Instantiate(Prefab1);
        fruit.SetPool(poolType1);
        return fruit;
    }
    public Fruit GetNextFruit(int number)
    {
        switch(number)
        {
            case 0:
                return CreateFruitType1();   
            case 1:
                return CreateFruitType2();
            case 2:
                return CreateFruitType3();
            default: return null;
        }
    }

    public Fruit GetFruitType1()
    {
        return poolType1.Get();
    }

    public Fruit GetFruitType2()
    {
        
        return poolType2.Get();
    }
    public Fruit GetFruitType3()
    {
        return poolType3.Get();
    }    
}
