using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class GameManager : Singleton<GameManager>
{
    public int score;
    private int highScore;

    private void Start()
    {
        score = 0;
    }

    public void IncreaseScore(int fruitTypeNumber)
    {
        switch(fruitTypeNumber)
        {
            case 0:
                score += 1;
                print(score);
                break;
            case 1:
                score += 2;
                print(score);
                break;
            case 2:
                score += 4;
                print(score);
                break;
        }
    }    
}
