using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShakeDetector : MonoBehaviour
{
    public float ShakeForceMultiplier;

    public float shakeDetectionThreshold;
    private float MinShakeInterval;

    private float sqrShakeDectetionThreshold;
    private float timeSinceLastShake;

    public void ShakeRigidbodies(Vector3 deviceAcceleration)
    {
        foreach(var rigidbody in FruitManager.instance.shakingRigidbodies)
        {
            rigidbody.AddForce(deviceAcceleration * ShakeForceMultiplier,ForceMode2D.Impulse);
        }
    }

    private void Start()
    {
        sqrShakeDectetionThreshold = Mathf.Pow(shakeDetectionThreshold, 2);
    }
    private void Update()
    {
        if(Input.acceleration.sqrMagnitude >= sqrShakeDectetionThreshold 
            && Time.unscaledTime >= timeSinceLastShake + MinShakeInterval)
        {
            ShakeRigidbodies(Input.acceleration);
            timeSinceLastShake = Time.unscaledTime;
        }
    }
}
